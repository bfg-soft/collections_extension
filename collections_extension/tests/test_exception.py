from pytest import raises

from collections_extension import ExceptionMap


# pylint: disable=missing-function-docstring


def test_exception_map():

    with raises(AssertionError):
        # noinspection PyTypeChecker
        ExceptionMap({1: 2})

    map_ = ExceptionMap({TypeError: 1, ValueError: 2})

    assert map_[TypeError] == 1
    assert map_.get(ValueError) == 2
    assert map_.get(RuntimeError) is None
    assert map_.get(RuntimeError, False) is False

    map_ = ExceptionMap({}, default_value=10)
    assert map_.get(TypeError) == 10
    assert map_.get(TypeError, 0) == 0
