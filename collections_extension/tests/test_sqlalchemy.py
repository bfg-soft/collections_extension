from contextlib import suppress

from pytest import skip

__all__ = [

]

# pylint: disable=import-outside-toplevel,missing-function-docstring

with suppress(ImportError):
    from sqlalchemy import String, Column
    from sqlalchemy.ext.declarative import declarative_base

    Base = declarative_base()

    # pylint: disable=too-few-public-methods,missing-class-docstring

    class Abstract(Base):
        __abstract__ = True

    class Model(Abstract):
        __tablename__ = 'model'
        name = Column(String, primary_key=True)

    class AnotherModel(Base):
        __tablename__ = 'another_model'
        name = Column(String, primary_key=True)


def test_sqlalchemy_model_map():
    try:
        from collections_extension import SqlalchemyModelMap

    except ImportError:
        skip('Sqlalchemy is not installed.')
        return

    model_map = SqlalchemyModelMap.from_module_name(__name__)

    assert frozenset(model_map) == {'model', 'another_model'}
    assert model_map['model'] is Model


def test_sqlalchemy_model_schema_map():
    try:
        from marshmallow import EXCLUDE

        from collections_extension import SqlalchemyModelSchemaMap, \
            SqlalchemyModelMap

    except ImportError:
        skip('Marshmallow-Sqlalchemy is not installed.')
        return

    model_map = SqlalchemyModelMap.from_module_name(__name__)
    schema_map = SqlalchemyModelSchemaMap.from_sqlalchemy_model_map(
        model_map,
        meta_kwargs={'unknown': EXCLUDE},
    )

    assert frozenset(model_map) == {'model', 'another_model'}

    data = schema_map['model'].load({'name': '1', 'test': 2})
    assert data == {'name': '1'}
